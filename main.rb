require 'net/http'
require 'mongo'
require 'json'

client = Mongo::Client.new("mongodb://127.0.0.1:27017/vademecum_price")
products = client[:product]
errors = client[:error]

File.open("assets/NadroCodes.csv", "r") do |f|
  f.each_line do |line|

    code = line.chop.chop
    puts code

    res = Net::HTTP.get(URI("https://api.byprice.com/item/#{code}"))
    product = JSON.parse(res)
    product.merge!("_id" => code)
    if !product.key?("code")

      res = Net::HTTP.get(URI("https://api.byprice.com/item/#{code}/history"))
      history = JSON.parse(res)
      product.merge!("history" => history)

      products.insert_one(product)

    else

      errors.insert_one(product)

    end

  end
end


